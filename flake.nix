# flake.nix
{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-23.11";
    flake-utils.url = "github:numtide/flake-utils";
  };
  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = nixpkgs.legacyPackages.${system};
      in
      {
        devShells.default = (pkgs.buildFHSUserEnv rec {
          name = "ESP-IDF Dev Shell";
          targetPkgs = pkgs: (with pkgs; [
#            (pkgs.callPackage ./esp32-toolchain.nix {})
	    libudev-zero
	    libusb
            git
            wget
            gnumake

            flex
            bison
            gperf
            pkg-config

            cmake
	    ccache
	    libffi
	    openssl
	    dfu-util
	    libz

            ncurses5

            ninja


	    platformio
	    esptool

	    # Must not use pythonX.withPackages here: Created venv will conflict with Espressif's venv
	    #python3
	    #python3Packages.pip
	    #python3Packages.virtualenv
	    #python3Packages.pyserial
	    (python3.withPackages(p: with p; [ pip setuptools pyserial ]))
          ]);
           #shellHook = ''
            #export IDF_PATH=$(pwd)/esp-idf
            #export PATH=$IDF_PATH/tools:$PATH
            #export IDF_PYTHON_ENV_PATH=$(pwd)/.python_env

            #if [ ! -e $IDF_PYTHON_ENV_PATH ]; then
            #  python -m venv $IDF_PYTHON_ENV_PATH
            #  . $IDF_PYTHON_ENV_PATH/bin/activate
            #  pip install -r $IDF_PATH/requirements.txt
            #else
            #  . $IDF_PYTHON_ENV_PATH/bin/activate
            #fi

	    #$IDF_PATH/install.fish

	    #echo Hi and welcome to my IDF shell!
          #'';
	  runScript = "fish";
        }).env;
      }
    );
}

